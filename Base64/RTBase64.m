/*
 * Copyright (c) 2001, Rude Tie, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *      -Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *
 *      -Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *      -Neither the name of the Rude Tie, LLC. nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 *
 *
 * Name:  RTBase64.m
 * Author/Date: Created by Jason Howk on 12/19/11.
 * Description: Base64 encoder/decoder.  Based on RFC 3548, 4648 
 * See: http://tools.ietf.org/html/rfc4648
 * 
 * Example: See associated logic tests for examples of use.
 */
#import "RTBase64.h"

#define INPUT_REGEX @"^[A-Za-z0-9+/]*[=]{0,2}" 
static unsigned const char b64Alphabet[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

@implementation RTBase64

+ (NSString *)encode:(NSData *)inputString
{
    NSInteger tmpSize = [inputString length];
    
    // Number of bits must be evenly divisible by 6.
    while ((tmpSize * 8) % 6 > 0) {
        tmpSize++;
    }
    // New array to hold the data.  Since it's a variable sized array
    // and larger than the source data, explicitly initialize it.
    char tmpAry[tmpSize];
    for (int x = 0; x < tmpSize; x++) {
        tmpAry[x] = 0;
    }
    
    // Start encoding.
    NSMutableString *result = [[[NSMutableString alloc] init] autorelease];
    [inputString getBytes:tmpAry length:tmpSize];
    for (int x = 0; x < tmpSize; x+=3) {
        int tuple = ((tmpAry[x] & 0xff) << 16) | ((tmpAry[x+1] & 0xff) << 8) | (tmpAry[x+2] & 0xff);
        // Break them down into 6-bit tuples and lookup the value;
        int offset = 0xfc0000;
        for (int y=3; y >= 0; y--) {
            if ( (x + (3-y) <= [inputString length]) && ((tuple & offset) >> 6 * y) > 0) {
                [result appendFormat:@"%c",b64Alphabet[(tuple & offset) >> 6 * y]];
            } else {
                // We're beyond the original size and into padding territory.  if it's a 0, it's a pad.
                [result appendString:@"="];
            }
            offset =  offset / 0x40;
        }
    }
    return result;
}

+(NSData *)decode:(NSString *)encodedInputString
{
    NSMutableData *output = [[[NSMutableData alloc] init] autorelease];
    NSString *input;
    
    // Reject any string that doesn't match the proper format.
    NSError *regexError;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:INPUT_REGEX 
                                                                           options:NSRegularExpressionCaseInsensitive 
                                                                             error:&regexError];
    if ([regex numberOfMatchesInString:encodedInputString options:0 range:NSMakeRange(0, [encodedInputString length])] < 1) {
        // It's not formatted properly.  Abort and return nil.
        return nil;
    }
    
    // Replace an = with an "A" (fixes any byte boundry issues we woudld see at the end of the encoded string.)
    if ([encodedInputString rangeOfString:@"="].location > 0) {
        input = [encodedInputString stringByReplacingOccurrencesOfString:@"=" withString:@"A"];
    } else {
        input = [[encodedInputString copy] autorelease];
    }
    
    // Start decoding.
    const char *tmpStr = [input UTF8String];
    for (int x = 0; x < strlen(tmpStr); x += 4) {
        char tmpAry[4] = {0};
        uint dbit = ((findChar(tmpStr[x]) & 0x3f) << 18)
        | ((findChar(tmpStr[x + 1]) & 0x3f) << 12)
        | ((findChar(tmpStr[x + 2]) & 0x3f) << 6)
        | ((findChar(tmpStr[x + 3]) & 0x3f));
               
        int offset = 0xff0000;
        int idx = 0;
        for (int q = 16; q >= 0; q -= 8) {
            uint bt = (dbit & offset) >> q;            
            // If the last quantum is 0 disregard it.
            if (x <= strlen(tmpStr)-4 && bt != 0) {
                //outputStr[idx] = bt;
                tmpAry[idx] = bt;
            }
            offset = offset / 0x100;
            idx++;
            if (idx > 3) {
                tmpAry[idx] = '\0';
            }
        }
        [output appendBytes:tmpAry length:strlen(tmpAry)];
    }
    return output;
}

#pragma mark - Private methods

int findChar(char c) {
    int result = 0;
    for (int x = 0; x < 64; x++) {
        if (b64Alphabet[x] == c) {
            return x;
        }
    }
    return result;
}
@end