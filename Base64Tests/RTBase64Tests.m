//
//  RTBase64Tests.m
//  RTBase64Tests
//
//  Created by Jason Howk on 12/19/11.
//  Copyright (c) 2011 Rude Tie, LLC. All rights reserved.
//

#import "RTBase64Tests.h"
#import "RTBase64.h"

@implementation RTBase64Tests

- (void)setUp
{
    [super setUp];
    // These are pulled from my Java base64 impl.
    input = [NSString stringWithString:@"The quick brown fox jumps over the lazy dog."];
    output = [NSString stringWithString:@"VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZy4="];
    // Per the RFC test vectors;
    rfcInput = [NSString stringWithString:@"foobar"];
    rfcOutput = [NSString stringWithString:@"Zm9vYmFy"];
    // Binary test.  
}

- (void)tearDown
{
    // Tear-down code here.    
    [super tearDown];
}

- (void)testQbfEncode
{
    NSData *data = [input dataUsingEncoding:NSASCIIStringEncoding];
    NSString *encodedStr = [RTBase64 encode:data];
    DLog(@"encodedStr: %@",encodedStr);
    STAssertEqualObjects(encodedStr, output, @"encoded output did not match reference data.");
}

- (void)testQbfDecode
{
    NSData *outputData = [RTBase64 decode:output];
    NSString *decodedStr = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
    DLog(@"decodedStr: %@",decodedStr);
    STAssertEqualObjects(decodedStr, input, @"Decoded output did not match reference data.");
    [decodedStr release];
}

- (void)testRfcEncode
{
    NSData *data = [rfcInput dataUsingEncoding:NSASCIIStringEncoding];
    NSString *encodedStr = [RTBase64 encode:data];
    DLog(@"encodedStr: %@",encodedStr);
    STAssertEqualObjects(encodedStr, rfcOutput, @"encoded output did not match reference data.");
}

- (void)testRfcDecode
{
    NSData *outputData = [RTBase64 decode:rfcOutput];
    NSString *decodedStr = [[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];
    DLog(@"decodedStr: %@",decodedStr);
    STAssertEqualObjects(decodedStr, rfcInput, @"Decoded output did not match reference data.");
    [decodedStr release];
}

@end
